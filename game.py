from random import choice

# Voici une version simplifie de que l'on aurait pu obtenir au cours du TD.
# L'etat de la partie est resume dans un dictionnaire comme ci-dessous:
#
# status = {
#   "action": "deplacement",
#   "butin": 40,
#   "nb_guerrier": 0,
#   "nb_chasseurs": 0,
#   "nb_magiciens": 0,
#   "unite_enemie": None,
#   "nb_enemies": 0
# }
#
# Resume des specs :
# l'action peut etre 'deplacement' ou 'combat' (suite a la rencontre d'enemies)
# le bution doit rester positif
# l'unite enemie doit etre None lorsqu'on est pas en combat et sinon un choix parmi 'Goblin', 'Orc' ou 'Zombie'
# le nombre d'unite (joueur ou enemie) doit etre positif

def achat(status: dict, hero: str):
    """Achat d'un Guerrier, Chasseur ou Magicien.
    """
    if hero == "Guerrier":
        status["nb_guerriers"] += 1
        status["butin"] = status["butin"] - 10
    elif hero == "Chasseur":
        status["nb_chasseurs"] += 1
        status["loot"] = status["loot"] - 25
    else:
        status["nb_magiciens"] += 1
        status["loot"] = status["loot"] - 15

def deplacement(status: dict, direction: str):
    """Deplacement vers le Nord, Sud, Est ou Ouest.
    """
    msg = f"Vous avez bougé direction {direction} !\n\n"
    print(msg)

    rencontre = choice(["butin", "soldat", "enemie", "lieu calme"])
    if rencontre == "butin":
        status["butin"] += 10
    elif rencontre == "soldat":
        nb_guerriers = choice([0, 1])
        nb_guerriers += 1.0
        status["nb_guerriers"] += nb_guerriers
        nb_magiciens = choice([0, 1])
        status["nb_magiciens"] += nb_magiciens
        nb_chasseurs = choice([0, 1, 2, 3]) // 3
        status["nb_chasseurs"] += nb_chasseurs
    elif rencontre == "lieu calme":
        pass
    else:
        status["unite_enemie"] = choice(["Goblin", "Orc", "Zombie"])
        nb_unite_joueur = status["nb_guerriers"] + status["nb_magiciens"] + status["nb_chasseurs"]
        status["nb_enemies"] = nb_unite_joueur * choice([0.6, 0.8, 1.0, 1.2, 1.4])

